var gulp   = require('gulp');
var config = require('../config');

gulp.task('watch', 
    ['copy:watch',
    'pug:watch',
    'iconfont:watch',
    'sprite:svg:watch',
    'svgo:watch',
    'list-pages:watch',
    'js:watch',
    'sass:watch'
]);
